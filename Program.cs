using System;
using System.Collections.Generic;

// Пример класса дома
class House
{
    public string Type { get; set; }
    public int Rooms { get; set; }
    public double Area { get; set; }
    // ... Другие свойства дома

    public HouseMediator mediator;

    public House(string type, int rooms, double area, HouseMediator mediator)
    {
        Type = type;
        Rooms = rooms;
        Area = area;
        this.mediator = mediator;
    }

    public virtual void Display()
    {
        Console.WriteLine($"Тип дома: {Type}, Комнаты: {Rooms}, Площадь: {Area} кв.м.");
    }

    public void SomeEvent()
    {
        // Вызов метода посредника при возникновении события
        mediator.HandleEvent(this);
    }
}

// Абстрактный строитель
abstract class HouseBuilder
{
    protected House house;

    public abstract void BuildType();
    public abstract void BuildRooms();
    public abstract void BuildArea();
    // ... Другие шаги строительства

    public House GetHouse()
    {
        return house;
    }
}

// Конкретный строитель дома
class ConcreteHouseBuilder : HouseBuilder
{
    private HouseMediator mediator;

    public ConcreteHouseBuilder(HouseMediator mediator)
    {
        this.mediator = mediator;
        house = new House(null, 0, 0, mediator);
    }

    public override void BuildType()
    {
        house.Type = "Кирпичный дом";
    }

    public override void BuildRooms()
    {
        house.Rooms = 3;
    }

    public override void BuildArea()
    {
        house.Area = 150.0;
    }

    // ... Реализация других шагов строительства
}

// Директор, который управляет процессом строительства дома
class HouseDirector
{
    private HouseBuilder builder;

    public HouseDirector(HouseBuilder builder)
    {
        this.builder = builder;
    }

    public void Construct()
    {
        builder.BuildType();
        builder.BuildRooms();
        builder.BuildArea();
    }
}

// Декоратор для добавления функциональностей к дому
abstract class HouseDecorator : House
{
    protected House house;

    public HouseDecorator(House house) : base(house.Type, house.Rooms, house.Area, house.mediator)
    {
        this.house = house;
    }

    public override void Display()
    {
        house.Display();
    }
}

// Пример конкретного декоратора для добавления "Умного дома"
class SmartHouseDecorator : HouseDecorator
{
    public SmartHouseDecorator(House house) : base(house)
    {
    }

    public override void Display()
    {
        base.Display();
        Console.WriteLine("Дополнительная функциональность: Умный дом");
    }
}

// Посредник для управления домами
class HouseMediator
{
    private List<House> houses = new List<House>();

    public void AddHouse(House house)
    {
        houses.Add(house);
    }

    public void DisplayHouses()
    {
        foreach (var house in houses)
        {
            house.Display();
        }
    }

    public void HandleEvent(House house)
    {
        // Обработка события в посреднике
        Console.WriteLine($"Посредник обработал событие от дома: {house.Type}");
    }
}

// Пример использования паттернов
class Program
{
    static void Main(string[] args)
    {
        // Создание посредника
        HouseMediator mediator = new HouseMediator();

        // Использование паттерна Строитель
        HouseBuilder builder = new ConcreteHouseBuilder(mediator);
        HouseDirector director = new HouseDirector(builder);
        director.Construct();
        House house = builder.GetHouse();

        // Использование паттерна Декоратор
        House decoratedHouse = new SmartHouseDecorator(house);

        // Добавление домов в посредник
        mediator.AddHouse(house);
        mediator.AddHouse(decoratedHouse);

        // Отображение всех домов с помощью посредника
        mediator.DisplayHouses();

        // Вызов события в доме
        house.SomeEvent();
    }
}
